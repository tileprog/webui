SHELL=/bin/bash

TARGET ?= rpi-taxibot
SSH = ssh $(TARGET)

.PHONY: run
run:
	python3 -m taxibot.main --port 8080

.PHONY: deploy
deploy:
	rsync \
    	-Carv \
    	--exclude=*.pyc \
    	--exclude=fixtures \
    	--exclude=*.http \
    	--exclude=*.map \
    	--exclude=.* \
    	--exclude=*.zip \
    	--exclude=Makefile \
    	--exclude=requirements-dev.txt \
    	--exclude=__pycache__ \
		--exclude=*.nodist \
    	. $(TARGET):

.PHONY: restart
restart:
	$(SSH) sudo systemctl restart taxibotd
	$(SSH) systemctl status taxibotd

.PHONY: status
status:
	$(SSH) systemctl status taxibotd

.PHONY: prepare
prepare:
	$(SSH) "sudo apt-get install python3-venv ; python3 -m venv .venv"
	$(SSH) ". .venv/bin/activate ; pip install -r requirements.txt"
