from pathlib import Path
import json

from taxibot.cooling import CoolingMode, CoolingSettings, Thresholds

SETTINGS_FILE_LOCAL_PATH = Path(".taxibot/settings.json")


class UserSettings:
    cooling: CoolingSettings

    def __init__(self):
        self._path = Path().home() / SETTINGS_FILE_LOCAL_PATH

    def check_dir(self):
        settings_dir = self._path.parent
        if not settings_dir.exists():
            settings_dir.mkdir(parents=True)

    @classmethod
    def get(cls) -> "UserSettings":
        settings = UserSettings()
        settings.load()
        return settings

    def load(self):
        if self._path.exists():
            with self._path.open() as fp:
                data = json.load(fp)

            self.cooling = CoolingSettings(
                mode=CoolingMode(data["cooling_mode"].lower()),
                auto_mode_thresholds=Thresholds(
                    on=data["cooling_auto_on"],
                    off=data["cooling_auto_off"]
                )
            )

    def as_dict(self) -> dict:
        return {
            "cooling_mode": self.cooling.mode.value,
            "cooling_auto_on": self.cooling.auto_mode_thresholds.on,
            "cooling_auto_off": self.cooling.auto_mode_thresholds.off,
        }

    def save(self):
        self.check_dir()

        with self._path.open("wt") as fp:
            json.dump(self.as_dict(), fp, indent=2)

    def update(self, cooling_mode=None, cooling_auto_on=None, cooling_auto_off=None):
        if cooling_mode:
            self.cooling.mode = CoolingMode(cooling_mode.lower())
        if cooling_auto_on:
            self.cooling.auto_mode_thresholds.on = int(cooling_auto_on)
        if cooling_auto_off:
            self.cooling.auto_mode_thresholds.off = int(cooling_auto_off)
