import os
from collections import namedtuple, OrderedDict
from enum import Enum
import pkg_resources
from typing import Callable, Optional

import numpy as np
import cv2

from .progmodel import Program, ProgramError, Instruction, Compiler, CompilationError
from .config import MIN_CONTOUR_WIDTH, MIN_CONTOUR_HEIGHT, MAX_CONTOUR_WIDTH, MAX_CONTOUR_HEIGHT

CV_MAJOR = int(cv2.__version__.split('.')[0])

MARKER_IMAGE_NAME = 'point.png'

SYMBOL_REF_IMAGES = [
    (Instruction.FWD, 'arrow_forward.png'),
    (Instruction.LFT, 'arrow_left.png'),
    (Instruction.RGT, 'arrow_right.png'),
    (Instruction.CPA, 'letter_A.png'),
    (Instruction.CPB, 'letter_B.png'),
    (Instruction.CPC, 'letter_C.png'),
    (Instruction.RP2, 'digit_2.png'),
    (Instruction.RP3, 'digit_3.png'),
    (Instruction.RP4, 'digit_4.png'),
]

DATA_PATH = pkg_resources.resource_filename(__package__, 'data/')
REF_IMAGES_DIR = os.path.join(DATA_PATH, 'refs')

# programming "grid" dimensions
ROWS = 4
COLUMNS = 5


COLOR_CONTOUR = (0, 200, 200)
COLOR_VALID_SYMBOL = (0, 127, 0)
COLOR_UNKNOWN_SHAPE = (0, 0, 255)
COLOR_BBOX = (255, 0, 0)


class BBox(namedtuple('BBox', 'x y w h')):
    @property
    def area(self):
        return self.w * self.h

    @property
    def center(self):
        return self.x + self.w // 2, self.y + self.h // 2

    def __str__(self):
        return "(x=%d, y=%d, w=%d, h=%d)" % (self.x, self.y, self.w, self.h)


ShapeData = namedtuple('ShapeData', 'bbox contour')


class State(Enum):
    UNDEFINED, \
    LOADED, ROTATED, GRAY_SCALED, ORIG_BLURRED, \
    ORIG_THRESHOLDED, ORIG_CONTOURED, \
    MARKERS_FOUND, \
    DEWARPED, \
    DEWARPED_THRESHOLDED, DEWARPED_CONTOURED, \
    RECOGNITON_DONE, \
    COMPLETE, \
    ERROR = range(14)

StateChangeCallback = Callable[[State, State], None]


class CVPipeline:
    _img_orig = None
    _img_gray = None
    _img_bw = None
    _img_display = None
    _img_display_bkgnd = None

    _contours = None
    _unwarp_matrix = None
    _unwarped_roi = None
    _unwarped_tblr = None
    _threshold = None

    _state = State.UNDEFINED
    _display_steps = False

    _error = None

    _cell_height = _cell_width = None

    _program = None       # type: Program

    @property
    def state(self) -> State:
        return self._state

    @state.setter
    def state(self, value: State):
        old_state = self._state

        self._state = value
        if self._state_callback:
            self._state_callback(old_state, self._state)

        if self._logger:
            self._logger.info("pipeline state changed to %s", self.state)

    @property
    def error(self) -> str:
        return self._error

    @property
    def image_original(self):
        return self._img_orig

    @property
    def image_work(self):
        if self._state in (State.ORIG_THRESHOLDED, State.DEWARPED_THRESHOLDED):
            return self._img_bw
        else:
            return self._img_gray

    @property
    def image_display(self):
        return self._img_display

    @property
    def contours(self):
        return self._contours

    @property
    def program(self) -> Program:
        return self._program

    def __init__(self, match_threshold, state_callback: StateChangeCallback = None, logger=None):
        self._state_callback = state_callback
        self._symbols_match_threshold = match_threshold
        self._display_steps = False
        self._logger = logger

        # load marker image for alignment
        self._img_dewarp_marker = cv2.cvtColor(
            cv2.imread(os.path.join(REF_IMAGES_DIR, MARKER_IMAGE_NAME)),
            cv2.COLOR_BGR2GRAY
        )

        # load "programing" symbols
        self._img_symbols = OrderedDict()
        for key, fname in SYMBOL_REF_IMAGES:
            img = cv2.imread(os.path.join(REF_IMAGES_DIR, fname), cv2.IMREAD_GRAYSCALE)
            self._img_symbols[key] = img

    def get_settings(self):
        return {
            "symbols_match_threshold": self._symbols_match_threshold
        }

    def initialize(self, img_path: str, display_steps: bool = False):
        self._display_steps = display_steps
        if not os.path.exists(img_path):
            raise Exception('image not found : %s' % img_path)

        self._img_display = self._img_orig = cv2.imread(img_path)

        if self._display_steps:
            self._img_display_bkgnd = self._img_orig.copy()

        self.state = State.LOADED

    def reset(self):
        self._img_orig = None
        self._img_gray = None
        self._img_bw = None
        self._img_display = None
        self._img_display_bkgnd = None
        self._contours = None
        self._threshold = None

        self.state = State.UNDEFINED
        self._error = None

    def rotate(self):
        self._img_display = self._img_orig = cv2.rotate(self._img_orig, cv2.ROTATE_180)
        self._img_display_bkgnd = self._img_orig.copy()

    def convert_to_grayscale(self):
        self._img_display = self._img_gray = cv2.cvtColor(self._img_orig, cv2.COLOR_BGR2GRAY)

    def blur(self):
        self._img_display = self._img_gray = cv2.GaussianBlur(self._img_gray, (5, 5), 0)

    def threshold(self):
        self._threshold, self._img_bw = cv2.threshold(self._img_gray, 60, 255, cv2.THRESH_BINARY_INV)
        self._img_display = self._img_bw.copy()

    def find_contours(self):
        if CV_MAJOR == 3:
            _, self._contours, _ = cv2.findContours(self._img_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        else:
            self._contours, _ = cv2.findContours(self._img_bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        if not self._contours:
            return "aucun contour trouvé"

        if self._display_steps:
            self._img_display = self._img_display_bkgnd.copy()

            for c in self._contours:
                bbox = BBox(*cv2.boundingRect(c))
                cv2.rectangle(self._img_display, (bbox.x, bbox.y), (bbox.x + bbox.w, bbox.y + bbox.h), COLOR_BBOX, 1)

            cv2.drawContours(self._img_display, self._contours, -1, COLOR_CONTOUR, 1)

    def locate_markers(self):
        markers = []
        to_be_drawn = []

        # analyze the shapes inside the extracted contours
        for ndx, c in enumerate(self._contours):
            bbox = BBox(*cv2.boundingRect(c))

            self._logger.debug("bbox size: w=%3d h=%3d", bbox.w, bbox.h)

            # exclude too small contours, since they are artefacts
            if bbox.w < MIN_CONTOUR_WIDTH or bbox.h < MIN_CONTOUR_HEIGHT:
                continue

            # extract the pixels inside the shape bounding box
            shape_img = self._img_bw[bbox.y:bbox.y + bbox.h, bbox.x:bbox.x + bbox.w]

            # resize the marker reference image to fit the bounding box
            resized_marker_img = cv2.resize(
                self._img_dewarp_marker,
                (bbox.w, bbox.h),
                interpolation=cv2.INTER_CUBIC
            )
            # and get the resulting mask
            _, marker = cv2.threshold(resized_marker_img, 60, 255, cv2.THRESH_BINARY_INV)
            marker = marker[:, :]

            # match the marker and the shape by XORing their masks,
            # the resulting image showing differences as white pixels
            xor_img = cv2.bitwise_xor(shape_img, marker)

            # count the black pixels for evaluating the match level (the higher, the better)
            match_level = sum(np.where(xor_img.ravel() == 0, 1, 0)) / bbox.area

            if match_level > 0.9:
                # we found a good candidate => keep it
                markers.append(bbox)
                to_be_drawn.append(c)

        if self._display_steps:
            self._img_display = self._img_orig.copy()
            cv2.drawContours(self._img_display, to_be_drawn, -1, (0, 255, 0), 1)

        # for marker in markers:
        #     self._logger.info("- %s", marker)

        markers_count = len(markers)
        if markers_count > 4:
            # we've got a problem finding the expected markers
            return f"trop de repères de recadrage ({markers_count})"
        if markers_count < 4:
            return f"pas assez de repères de recadrage ({markers_count})"

        # The reference points coordinates are defined by the innermost corners of
        # the markers bounding box with respect to the center of the image.
        # This defines an area which excludes the markers, so that they will not
        # interfere with the rest of the image processing.

        # Compute the approximated center of this area.
        # Not a 100% accurate result since the min and max strategy does not give
        # the innermost points. But this will precise enough for the usage of this center.
        cx = (max(m.x for m in markers) + min(m.x + m.w for m in markers)) // 2
        cy = (max(m.y for m in markers) + min(m.y + m.h for m in markers)) // 2

        # identifies the corners of source area in the image to be stitched
        # (strictly inside the markers)
        tl = tr = bl = br = None
        for m in markers:
            if m.x < cx:
                if m.y < cy:
                    tl = (m.x + m.w, m.y + m.h)
                else:
                    bl = (m.x + m.w, m.y)
            else:
                if m.y < cy:
                    tr = (m.x, m.y + m.h)
                else:
                    br = (m.x, m.y)

        # now we can get the true t/l/b/r values.
        t = max(tl[1], tr[1])
        b = min(bl[1], br[1])
        l = max(tl[0], bl[0])
        r = min(tr[0], br[0])

        src_corners = [tl, tr, bl, br]

        # the destination area has its corners properly aligned on X and Y axis
        self._unwarped_tblr = t, b, l, r
        self._unwarped_roi = [(l, t), (r, t), (l, b), (r, b)]
        self._unwarp_matrix = cv2.getPerspectiveTransform(np.float32(src_corners), np.float32(self._unwarped_roi))

    def dewarp_image(self):
        # self._assert_state(self.State.MARKERS_FOUND)

        t, b, l, r = self._unwarped_tblr
        dewarped_size = self._img_orig.shape[:2][::-1]
        self._img_gray = cv2.warpPerspective(self._img_gray, self._unwarp_matrix, dewarped_size)[t:b, l:r]

        img_height, img_width = self._img_gray.shape[:2]
        self._cell_width = img_width // COLUMNS
        self._cell_height = img_height // ROWS

        if self._display_steps:
            self._img_display = self._img_gray.copy()
            self._img_display_bkgnd = cv2.warpPerspective(self._img_orig, self._unwarp_matrix, dewarped_size)[t:b, l:r]

    def detect_symbols(self):
        # Detected shapes data will be stored in the following map, keyed by the (X, Y) position
        # of the cell in the grid.
        # We keep the bounding box and the contour, stored as a named tuple (ShapeData).
        # Note: only the bounding box is needed for the recognition step, contours being
        # kept only for the display.
        extracted_shapes = {}

        # Analyze all the contours
        for ndx, c in enumerate(self._contours):
            # Discard two short ones (at least 3 points are required to form a not null surface)
            c_lg = len(c)
            if c_lg < 3:
                continue

            # Compute the center of the contour
            bbox = BBox(*cv2.boundingRect(c))

            # Discard too small contours that are probably shadows
            if bbox.w < MIN_CONTOUR_WIDTH or bbox.h < MIN_CONTOUR_HEIGHT:
                continue

            # same for too large ones than cannot be valid symbols
            if bbox.w > MAX_CONTOUR_WIDTH or bbox.h > MAX_CONTOUR_HEIGHT:
                continue

            # Compute the cell coordinates, located in the center of the shape bounding box
            c_x, c_y = bbox.center
            row, col = c_y // self._cell_height, c_x // self._cell_width

            # Check if the new shape belongs to a cell for which something has already been found.
            # If yes, and if the new shape is bigger than the previous one, have it replace
            # the old one. If not, just discard it.

            try:
                current_area = extracted_shapes[row, col].bbox.area
            except KeyError:
                current_area = 0

            if bbox.w * bbox.h > current_area:
                # The new shape is bigger than the one we already have (if any)
                # => replace it
                extracted_shapes[row, col] = ShapeData(bbox, c)

        # All the candidate shapes have been extracted now. Do some display and reporting before
        # analyzing them.

        # Symbols recognition step

        # Build the program by scanning the board by row/col order.

        self._program = Program()

        # text settings for annotations on the image
        font_face = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.4
        font_thickness = 1

        # prepare the display background if we have to
        if self._display_steps:
            self._img_display = self._img_display_bkgnd.copy()

        for row, col in sorted(extracted_shapes):
            bbox = extracted_shapes[row, col].bbox

            # Extract the cell pixels
            shape_img = self._img_bw[bbox.y:bbox.y + bbox.h, bbox.x:bbox.x + bbox.w]

            # Assess the similarity of each symbol with the shape. The resulting matching indicator
            # is kept for each assessed symbol.
            matching_indicators = {}
            for key, symbol_img in self._img_symbols.items():
                # Resize the symbol image to match the cell shape
                symbol_img = cv2.resize(
                    symbol_img,
                    (bbox.w, bbox.h),
                    interpolation=cv2.INTER_CUBIC
                )
                _, symbol_img = cv2.threshold(symbol_img, 60, 255, cv2.THRESH_BINARY_INV)

                # Get the pixels of the resized symbol
                symbol_img = symbol_img[:, :]

                # Match the cell and the symbol bitmaps by XORing them. Pixels matching
                # in both image produce black pixels in the resulting image.
                xor_img = cv2.bitwise_xor(shape_img, symbol_img)

                # The matching indicator is the number of black pixels in the XOR result
                matching_indicators[key] = sum(np.where(xor_img.ravel() == 0, 1, 0))
                # self._logger.info("[%d,%d] %s match indicator : %.2f", row, col, key, matching_indicators[key])

            # Find the symbol which matches the best and compute its match level as
            # the ratio of identical pixels versus the area of the shape bounding box.
            best_match = max(matching_indicators, key=matching_indicators.get)
            match_level = matching_indicators[best_match] / bbox.area

            # Keep only candidates with a good enough match level

            bbox_center = bbox.center
            text_y = bbox.y + bbox.h + 15

            if match_level >= self._symbols_match_threshold:
                # Add the symbol to the program sheet, in the sequence corresponding
                # to the row being scanned
                if row == 0:
                    sequence = self._program.main
                else:
                    sequence = self._program.sub_programs['ABCD'[row - 1]]

                sequence.append(best_match)

                # configure the display to show that this shape is a valid one
                annotation = f"{best_match.name} ({match_level:.2f})"
                bbox_color = COLOR_VALID_SYMBOL
                text_color = (0, 0, 0)

            else:
                # Record the error
                if row == 0:
                    part = 'main'
                else:
                    part = 'ABC'[row - 1]
                self._program.errors[part].append(ProgramError(col, "unknown symbol"))

                # configure the display to show that this shape is not a known one
                annotation = f"?? ({match_level:.2f})"
                bbox_color = COLOR_UNKNOWN_SHAPE
                text_color = (0, 0, 0)

            # do some display now
            if self._display_steps:
                cv2.rectangle(self._img_display, (bbox.x, bbox.y), (bbox.x + bbox.w, bbox.y + bbox.h),
                              bbox_color, 1)

                text_size, _ = cv2.getTextSize(annotation, font_face, font_scale, font_thickness)
                text_w, text_h = text_size

                text_x = bbox_center[0] - text_w // 2
                padding_x = 2
                padding_y = 4

                cv2.rectangle(
                    self._img_display,
                    (text_x - padding_x, text_y - text_h - padding_y),
                    (text_x + text_w + padding_x, text_y + padding_y),
                    bbox_color,
                    cv2.FILLED
                )
                cv2.putText(
                    self._img_display,
                    annotation,
                    (text_x, text_y),
                    font_face,
                    font_scale,
                    text_color,
                    font_thickness,
                    cv2.LINE_AA
                )

    _fsm = {
        State.LOADED: (rotate, State.ROTATED),
        State.ROTATED: (convert_to_grayscale, State.GRAY_SCALED),
        State.GRAY_SCALED: (blur, State.ORIG_BLURRED),
        State.ORIG_BLURRED: (threshold, State.ORIG_THRESHOLDED),
        State.ORIG_THRESHOLDED: (find_contours, State.ORIG_CONTOURED),
        State.ORIG_CONTOURED: (locate_markers, State.MARKERS_FOUND, State.ERROR),
        State.MARKERS_FOUND: (dewarp_image, State.DEWARPED),
        State.DEWARPED: (threshold, State.DEWARPED_THRESHOLDED),
        State.DEWARPED_THRESHOLDED: (find_contours, State.DEWARPED_CONTOURED, State.ERROR),
        State.DEWARPED_CONTOURED: (detect_symbols, State.COMPLETE, State.ERROR),
        State.ERROR: (lambda _: False, State.ERROR, State.ERROR),
    }

    def run(self, auto=True):
        while self.state not in (State.COMPLETE, State.ERROR):
            self.step()

        self.log_program()
        return self.state == State.COMPLETE

    def step(self):
        transition, success_state, error_state = (self._fsm[self._state] + (None,))[:3]

        error = transition(self)
        if not error:
            self.state = success_state
        else:
            self._logger.error(error)
            if error_state:
                self.state = error_state
                self._error = error
            else:
                raise Exception("%s failure" % transition.__name__)

    def log_program(self):
        if not self._logger:
            return

        if self._error:
            self._logger.warning(f"program analyze failed : {self._error}")
            return

        pb = self._program
        if not pb:
            self._logger.warning("could not analyze the program")
            return

        self._logger.info("recognized program :")
        self._logger.info("- main : %s", [s.name for s in pb.main])
        for sp in 'ABC':
            self._logger.info("- sp-%s : %s", sp, [s.name for s in pb.sub_programs[sp]])

        compiler = Compiler(pb)
        try:
            moves = compiler.generate_sequence()
        except CompilationError as e:
            self._logger.info("program is invalid : %s", e)
        else:
            self._logger.info("%d moves : %s", len(moves), moves)

