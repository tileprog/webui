#!/usr/bin/env python3
import argparse

from taxibot.webapp import runserver

parser = argparse.ArgumentParser(description='Start the webapp server')
parser.add_argument('--port', '-p', type=int, default=8080, help='server port to listen to')
args = parser.parse_args()

runserver(args.port)
