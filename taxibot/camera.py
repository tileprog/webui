import os
import shutil
import subprocess
from fractions import Fraction
import time

import pkg_resources

from . import config
from .log import get_logger

__author__ = 'Eric Pascual'

__all__ = ['CameraManager', 'frac_to_float', 'CAPTURED_IMAGE_PATH']

logger = get_logger("camera")

CAPTURED_IMAGE_PATH = '/tmp/taxibot-picture.jpg'


class ResolutionMixin:
    _resolution = (1024, 768)

    @property
    def resolution(self):
        return self._resolution

    @resolution.setter
    def resolution(self, value):
        self._resolution = value


class TestFilePiCamera(ResolutionMixin):
    def __init__(self, test_file_path):
        if not os.path.exists(test_file_path):
            raise ValueError(f'test file not found: {test_file_path}')
        self.test_file_path = test_file_path

    def capture(self, image_path):
        logger.info(f"returning test image {self.test_file_path}")
        shutil.copyfile(self.test_file_path, image_path)

    def close(self):
        pass


class RemotePiCamera(ResolutionMixin):
    def __init__(self, host_name):
        self.camera_host = host_name

    def close(self):
        pass

    def capture(self, image_path):
        logger.info(f"retrieving real image from remote camera on {self.camera_host}")
        result = subprocess.run(
            ["ssh", "rpi-taxibot", "python3 ./taxibot/capture.py"],
            check=True,
            capture_output=True,
            text=True
        )
        logger.info("Output from remote capture script:")
        for line in result.stdout.splitlines():
            logger.info("... %s", line)

        subprocess.run(["scp", "rpi-taxibot:test.jpg", image_path], check=True)
        logger.info("captured file transferred locally")


def make_test_camera():
    image_name = config.TEST_IMAGE_NAME
    logger.warning(f"Using mocked camera with test image '{image_name}'.")

    test_image_path = pkg_resources.resource_filename('taxibot', 'data/fixtures/' + image_name)
    return TestFilePiCamera(test_image_path)


def frac_to_float(frac: Fraction) -> float:
    return round(frac.numerator / frac.denominator, 2)


class CameraManager:
    camera = None

    @classmethod
    def get_camera(cls):
        if cls.camera is None:
            cls.make_camera()
        return cls.camera

    @classmethod
    def release_camera(cls):
        if cls.camera is not None:
            cls.camera.close()
            cls.camera = None

    @classmethod
    def make_camera(cls):
        if config.CAPTURE_MODE == config.CaptureMode.NORMAL:
            try:
                from picamera import PiCamera

            except ImportError:
                logger.warning("PiCamera not available. Falling back to test mode.")
                config.CAPTURE_MODE = config.CaptureMode.TEST
                return make_test_camera()

            else:
                cls.release_camera()

                _camera = PiCamera()

                # let the camera calibrate itself and then disable automatic modes
                # to speed up subsequent captures
                logger.info("camera calibrating...")
                time.sleep(config.CAMERA_CALIBRATION_DELAY)

                _camera.exposure_mode = 'off'

                logger.info(
                    'auto exposure turned off (digital_gain=%.2f analog_gain=%.2f)',
                    frac_to_float(_camera.digital_gain),
                    frac_to_float(_camera.analog_gain)
                )

                calibrated_awb_gains = _camera.awb_gains
                logger.info(
                    'AWB turned off (red=%.2f blue=%.2f)',
                    frac_to_float(calibrated_awb_gains[0]),
                    frac_to_float(calibrated_awb_gains[1]),
                )
                _camera.awb_mode = 'off'
                _camera.awb_gains = calibrated_awb_gains

                cls.camera = _camera

        elif config.CAPTURE_MODE == config.CaptureMode.REMOTE:
            camera_host = config.CAMERA_HOST
            logger.warning(f"using remote camera on {camera_host}")

            cls.camera = RemotePiCamera(camera_host)

        elif config.CAPTURE_MODE == config.CaptureMode.TEST:
            cls.camera = make_test_camera()

        return cls.camera

    @classmethod
    def recreate_camera(cls):
        cls.camera = cls.make_camera()
        return cls.camera


CameraManager.make_camera()
