from collections import defaultdict, namedtuple, deque
from enum import Enum

__author__ = 'Eric Pascual'


class Instruction(Enum):
    FWD = ('F', None, None)
    LFT = ('L', None, None)
    RGT = ('R', None, None)
    CPA = (None, 'A', None)
    CPB = (None, 'B', None)
    CPC = (None, 'C', None)
    RP2 = (None, None, 2)
    RP3 = (None, None, 3)
    RP4 = (None, None, 4)

    def __init__(self, move_value, called_seq, repeat_count):
        self.move_value = move_value
        self.called_seq = called_seq
        self.repeat_count = repeat_count

    @property
    def is_move(self) -> bool:
        return self.move_value is not None

    @property
    def is_call(self) -> bool:
        return self.called_seq is not None

    @property
    def is_repeat(self) -> bool:
        return self.repeat_count is not None


class Program:
    def __init__(self):
        self.main = []
        self.sub_programs = defaultdict(list)
        self.errors = defaultdict(list)

    def as_dict(self):
        return {
            'main': self.main,
            'sub_programs': self.sub_programs,
            'errors': self.errors
        }

    @staticmethod
    def serialize_sequence(seq):
        return [stmt.name for stmt in seq]

    def serialize(self):
        return {
            'main': self.serialize_sequence(self.main),
            'sub_programs': {name: self.serialize_sequence(seq) for name, seq in self.sub_programs.items()},
            'errors': self.errors,
        }

    def as_table(self):
        def pad_sequence(s):
            return (s + [None] * 5)[:5]

        return [
            pad_sequence(self.main),
            pad_sequence(self.sub_programs['A']),
            pad_sequence(self.sub_programs['B']),
            pad_sequence(self.sub_programs['C'])
        ]


ProgramError = namedtuple('ProgramError', 'col message')


class CompilationError(Exception):
    def __init__(self, errors):
        self.errors = errors


class CallCheckError(Exception):
    pass


class Compiler:
    def __init__(self, pgm: Program):
        self._pb = pgm

    def check_call_self(self, sp_name: str, sp_stmts: list):
        for call in (stmt for stmt in sp_stmts if stmt.is_call):
            if call.called_seq == sp_name:
                raise CallCheckError()

    def generate_sequence(self) -> str:
        errors = []

        for sp_name, sp_stmts in sorted(self._pb.sub_programs.items()):
            try:
                self.check_call_self(sp_name, sp_stmts)
            except CallCheckError:
                errors.append(f"La procédure {sp_name} s'appelle elle-même")

        moves_seq = []
        call_stack = deque()
        undefined = []

        def parse_sequence(seq):
            repeat_count = 1

            for stmt in seq:
                if stmt.is_call:
                    called = stmt.called_seq
                    if called in call_stack:
                        errors.append(f"Appel cyclique de la procédure {called}")
                        continue

                    if self._pb.sub_programs[called]:
                        call_stack.append(called)
                        for _ in range(repeat_count):
                            parse_sequence(self._pb.sub_programs[called])
                        repeat_count = 1
                        call_stack.pop()

                    else:
                        if called not in undefined:
                            errors.append(f"La procédure {called} n'est pas définie")
                            undefined.append(called)

                elif stmt.is_move:
                    moves_seq.extend([stmt.move_value] * repeat_count)
                    repeat_count = 1

                elif stmt.is_repeat:
                    repeat_count = stmt.repeat_count

                else:
                    errors.append(f"Instruction non supportée: {stmt.name}")

            if repeat_count > 1:
                errors.append("Répétition sans instruction à répéter")

        parse_sequence(self._pb.main)

        if len(self._pb.main) == 0:
            errors.append('Ce programme ne fait rien')

        if errors:
            raise CompilationError(errors)

        return ''.join(moves_seq)
