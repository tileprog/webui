from enum import Enum

from environs import Env
from marshmallow.validate import OneOf


class CaptureMode(Enum):
    NORMAL = 'normal'
    REMOTE = 'remote'
    TEST = 'test'

    @classmethod
    def choices(cls):
        return [e.value for e in cls]


DEFAULT_SYMBOLS_MATCH_THRESHOLD = 0.85


env = Env()
env.read_env()


with env.prefixed("TAXIBOT_"):
    DEBUG = env.bool("DEBUG", default=False)

    CAPTURE_MODE = CaptureMode(env.str(
        "CAPTURE_MODE",
        default=CaptureMode.NORMAL.value,
        validate=OneOf(
            CaptureMode.choices(),
            error="TAXIBOT_CAPTURE_MODE must be one of: {choices}"
        )
    ))
    TEST_IMAGE_NAME = env("TEST_IMAGE_NAME", default="test.jpg")
    CAMERA_HOST = env("CAMERA_HOST", default="rpi-taxibot")
    CAMERA_CALIBRATION_DELAY = env.int("CAMERA_CALIBRATION_DELAY", default=2)
    CAMERA_RELEASE_ALWAYS = env.bool("CAMERA_RELEASE_ALWAYS", default=False)

    BOT_SERIAL_PORT = env("BOT_SERIAL_PORT")
    BOT_SERIAL_BAUD = env.int("BOT_SERIAL_BAUD", default=9600)

    SYMBOLS_MATCH_THRESHOLD = env.float("SYMBOLS_MATCH_THRESHOLD", default=DEFAULT_SYMBOLS_MATCH_THRESHOLD)

    SHUTDOWN_CONFIRMATION_CODE = env("SHUTDOWN_CONFIRMATION_CODE")
    SHUTDOWN_DELAY = env.int("SHUTDOWN_DELAY", default=3)
    SHUTDOWN_SIMULATED = env.bool("SHUTDOWN_SIMULATED", default=False)

    CPU_THERMAL_ZONE = env.int("CPU_THERMAL_ZONE", default=0)

    MIN_CONTOUR_WIDTH = env.int("MIN_CONTOUR_WIDTH", default=10)
    MIN_CONTOUR_HEIGHT = env.int("MIN_CONTOUR_HEIGHT", default=10)
    MAX_CONTOUR_WIDTH = env.int("MAX_CONTOUR_WIDTH", default=75)
    MAX_CONTOUR_HEIGHT = env.int("MAX_CONTOUR_HEIGHT", default=75)
