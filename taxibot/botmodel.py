import logging
from enum import Enum
import threading
# from queue import Queue, Empty
from collections import deque
from functools import reduce
import time

from .progmodel import Program

__author__ = 'Eric Pascual'


logger = logging.getLogger('bot')


class FakePort:
    timeout = 1

    def write(self, data):
        logger.warning("simulated serial write (data=%s)", data)

    def read(self):
        raise NotImplementedError()

    def reset_input_buffer(self):
        pass

    def readline(self):
        raise NotImplementedError()


class BotCommunicationError(Exception):
    pass


class BotState(Enum):
    READY = 0
    FOLLOWING_LINE = 1
    LOST_LINE = 2
    TURNING = 3
    AT_CROSSING = 4
    LEAVING_CROSSING = 5
    TURNING_STAGE_1 = 6
    TURNING_STAGE_2 = 7
    BLOCKED = 8
    PANIC = 9


class RxStreamReader(threading.Thread):
    def __init__(self, bot, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._bot = bot
        self._port = bot.port
        self._terminate = False
        self.replies_queue = deque()

        self.on_event = None
        self.on_reply = None

    def run(self) -> None:
        logger.info("starting Rx thread...")
        self._terminate = False
        while not self._terminate:
            rx_packet = self._port.readline()
            if rx_packet:
                try:
                    rx_packet = rx_packet.strip().decode()
                except Exception:
                    logger.warning("Rx error, resetting input")
                    self._port.reset_input_buffer()
                    rx_packet = None

                if not rx_packet:
                    continue

                logger.info("Rx: %s", rx_packet)

                header = rx_packet[0]
                if header == '#':
                    continue

                payload = rx_packet[1:]
                if header == '<':
                    self.replies_queue.append(payload)
                    logger.info('added to replies queue : %s (Qsize=%d)', payload, len(self.replies_queue))
                elif header == '!':
                    logger.info('event received : %s', payload)
                    if self.on_event:
                        logger.debug('%s event handler called', self.on_event)
                        self.on_event(payload)
                        logger.debug('%s returned', self.on_event)
                else:
                    logger.error('invalid Rx packet: %s', rx_packet)

        logger.info("Rx terminated")

    def terminate(self):
        self._terminate = True

    def flush_rx(self):
        self._port.reset_input_buffer()
        self.replies_queue.clear()


class Bot:
    def __init__(self, port: str, baudrate: int = 9600):
        try:
            import serial
            self._port = serial.Serial(port=port, baudrate=baudrate, timeout=1)
        except (ImportError, serial.SerialException) as e:
            logger.error('serial com error : %s', e)
            logger.warning('communications will be simulated from now.')
            self._port = FakePort()

        self._rx_thread = None

        self._ws = None
        self._io_loop = None

    @property
    def port(self):
        return self._port

    @property
    def websocket(self):
        return self._ws

    @websocket.setter
    def websocket(self, value):
        self._ws = value
        logger.debug("websocket is set to %s", self._ws)

    def on_event(self, data):
        logger.debug("Bot.on_event called (self._ws = %s)", self._ws)
        if self._ws:
            logger.debug("writing message to websocket...")
            self._io_loop.add_callback(self._ws.write_message, f'EVT {data}')
            logger.debug("message sent")

    def ready(self, io_loop):
        self._io_loop = io_loop
        self.open_rx_stream()

    def shutdown(self):
        self.close_rx_stream()

    def __del__(self):
        self.shutdown()

    def send_command(self, command):
        self._rx_thread.flush_rx()

        command = command.strip()
        logger.info("Tx: %s", command)

        self._port.write(command.encode())
        self._port.write(b'\x0a')

    def _raise_communication_error(self, msg):
        logger.error(msg)
        raise BotCommunicationError(msg)

    def open_rx_stream(self):
        if isinstance(self._port, FakePort):
            logger.warning('Rx thread not available for FakeBot')
            return

        if self._rx_thread:
            raise BotCommunicationError('Rx stream already opened')

        self._rx_thread = RxStreamReader(self)
        self._rx_thread.on_event = self.on_event
        self._rx_thread.start()

    def close_rx_stream(self):
        if isinstance(self._port, FakePort):
            return

        if not self._rx_thread:
            raise BotCommunicationError('Rx stream not opened')

        self._rx_thread.terminate()
        self._rx_thread.join(10)
        self._rx_thread = None

    def _process_reply(self, reply):
        fields = reply.split(' ', maxsplit=2)
        if len(fields) < 2:
            self._raise_communication_error(f"malformed reply received ({reply})")

        status, command = fields[:2]

        if status == 'OK':
            try:
                return fields[2]
            except IndexError:
                return None

        elif status == 'INV':
            self._raise_communication_error(f"invalid command reply received ({reply})")

    def get_reply(self):
        time_limit = time.time() + self._port.timeout
        queue = self._rx_thread.replies_queue
        while time.time() <= time_limit:
            if len(queue) > 0:
                reply = self._rx_thread.replies_queue.popleft()
                logger.info("reply: %s", reply)
                return self._process_reply(reply)

            time.sleep(0.1)

        self._raise_communication_error("no reply available")

    def execute_moves(self, sequence):
        if len(sequence) > 1:
            self.send_command('$' + sequence)
        else:
            self.send_command(sequence)

    def execute_program(self, pgm: Program):
        self.send_command('*' + reduce(lambda a, b: a + b, pgm.as_table()))

    def stop(self):
        self.send_command('S')

    def reset(self):
        self.send_command('!')

    def get_state(self) -> BotState:
        self.send_command('?S')

        reply = self.get_reply()
        if reply:
            return BotState(int(reply))
        else:
            raise BotCommunicationError("empty reply for ?S")

    def get_line_sensors(self):
        sensor_states = {}

        # get front sensor
        self.send_command('?F')

        reply = self.get_reply()
        if reply:
            bits = int(reply)
            sensor_states['front'] = [bool(bits & (1 << pos)) for pos in range(2)]

        else:
            raise BotCommunicationError("empty reply for ?F")

        # get axle sensor
        self.send_command('?A')

        reply = self.get_reply()
        if reply:
            bits = int(reply)
            sensor_states['axle'] = [bool(bits & (1 << pos)) for pos in range(6)]

        else:
            raise BotCommunicationError("empty reply for ?A")

        return sensor_states

    def get_distance_sensor(self):
        self.send_command('?D')

        reply = self.get_reply()
        if reply:
            return int(reply)
        else:
            raise BotCommunicationError("empty reply for ?D")

    def get_obstacle(self):
        self.send_command('?O')

        reply = self.get_reply()
        if reply:
            return int(reply)
        else:
            raise BotCommunicationError("empty reply for ?0")
