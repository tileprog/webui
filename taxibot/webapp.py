import os
import json
import logging
import subprocess
from pathlib import Path
from time import time

import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpclient
import tornado.escape

import cv2

from . import config
from .botmodel import Bot, BotCommunicationError
from .camera import CameraManager, CAPTURED_IMAGE_PATH, frac_to_float
from .log import get_logger, log_settings, app_logger
from .user_settings import UserSettings

from .pipeline import CVPipeline, State
from .progmodel import Compiler, CompilationError
from .cooling import CoolingController, CoolingMode

from . import __VERSION__

__here__ = os.path.dirname(__file__)

pkg_dir = os.path.dirname(__file__)
project_root = os.path.dirname(pkg_dir)

logger = get_logger("webapp")
logger.setLevel(logging.DEBUG if config.DEBUG else logging.INFO)

app_context = {
    'app_title': "TaxiBot",
    'version': __VERSION__
}


class ImageCaptureMixin:
    def acquire_image(self):
        """ Capture an image with the camera, store it locally under the path set in ``captured_image_path``.
        """
        logger.info('image capture requested...')

        camera = CameraManager.get_camera()
        try:
            start_time = time()
            camera.capture(CAPTURED_IMAGE_PATH)
            elapsed = time() - start_time

            logger.info('complete (capture time=%.3fs)', elapsed)

        finally:
            if config.CAMERA_RELEASE_ALWAYS:
                CameraManager.release_camera()

        return CAPTURED_IMAGE_PATH


class BotMixin:
    _bot = None  # type: Bot

    def initialize(self, **kwargs):
        self._bot = kwargs['bot']

    def ws_opened(self, ws: tornado.websocket.WebSocketHandler):
        self._bot.websocket = ws

    def ws_closed(self):
        self._bot.websocket = None


class PipelineMixin:
    _pipeline = None  # type: CVPipeline

    def initialize(self, **kwargs):
        self._pipeline = kwargs['pipeline']


class HomeHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(
            "home.html",
            current_page="home",
            **app_context
        )


class ProgrammingHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(
            "prog.html",
            current_page="prog",
            **app_context
        )


class ProgramAnalyzeHandler(ImageCaptureMixin, PipelineMixin, tornado.web.RequestHandler):
    def get(self):
        start_time = time()
        try:
            image_path = self.acquire_image()

        except Exception as e:
            logger.error('cannot acquire board picture: %s', e)
            result = {
                'error': "impossible de capturer l'image du programme"
            }

        else:
            now = time()
            capture_time = now - start_time
            start_time = now

            logger.info(f"initializing pipeline with image {image_path}")
            self._pipeline.initialize(image_path, display_steps=True)

            if self._pipeline.run():
                analyze_time = time() - start_time
                logger.info('analyze successful')

                pb = self._pipeline.program
                result = {
                    'pgm': pb.serialize(),
                    'stats': {
                        'capture_time': capture_time,
                        'analyze_time': analyze_time
                    }
                }

                compiler = Compiler(pb)
                try:
                    result['moves'] = compiler.generate_sequence()
                except CompilationError as e:
                    result['compile_errors'] = e.errors

            else:
                error = self._pipeline.error
                logger.warning(f'analyze failed: {error}')
                result = {
                    'error': error
                }

        self.write(json.dumps(result))
        self.set_header("Cache-control", "no-store")


class ProgramBoardImageHandler(PipelineMixin, tornado.web.RequestHandler):
    def get(self):
        _, encoded_img = cv2.imencode('.jpg', self._pipeline.image_display)
        self.write(encoded_img.tostring())
        self.set_header("Content-type", "image/jpg")
        self.set_header("Cache-control", "no-store")


class BotMoveHandler(BotMixin, tornado.web.RequestHandler):
    def post(self):
        try:
            moves = self.get_body_argument('moves')
        except tornado.web.MissingArgumentError as e:
            self.set_status(400, reason=e.log_message)
            return

        if moves:
            logger.info("sending moves sequence to bot: %s", moves)
            self._bot.execute_moves(moves)

            self.write(json.dumps({
                "success": True
            }))
            self.set_status(201)

        else:
            self.write(json.dumps({
                "success": False,
                "error": "ERR_NOMOVE"
            }))
            self.set_status(422)


class BotStopHandler(BotMixin, tornado.web.RequestHandler):
    def post(self):
        logger.info('stopping bot')
        self._bot.stop()

        self.write(json.dumps({
            "success": True
        }))


class BotControlHandler(BotMixin, tornado.web.RequestHandler):
    def get(self):
        self.render(
            "botctrl.html",
            current_page="bot",
            **app_context
        )


class BotGetStateHandler(BotMixin, tornado.web.RequestHandler):
    def get(self):
        logger.info('getting bot state')

        state = self._bot.get_state()
        self.write(json.dumps({
            "state": state.name
        }))
        self.set_header("Cache-control", "no-store")


class BotGetSensorsHandler(BotMixin, tornado.web.RequestHandler):
    def get(self):
        logger.info('getting bot sensors reading')

        try:
            sensors = {
                "line": self._bot.get_line_sensors(),
                "dist": self._bot.get_distance_sensor()
            }

            self.write(json.dumps(sensors))
            self.set_header("Cache-control", "no-store")

        except BotCommunicationError as e:
            self.write(json.dumps({
                "success": False,
                "error": "ERR_COMM"
            }))
            self.set_status(422)


class BotGetObstacleHandler(BotMixin, tornado.web.RequestHandler):
    def get(self):
        logger.info('getting bot obstacle detection reading')

        self.write(json.dumps({
            "obstacle": self._bot.get_obstacle()
        }))
        self.set_header("Cache-control", "no-store")


class SystemCommandsHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(
            "system.html",
            current_page="system",
            **app_context
        )


def _shutdown_system():
    if not config.SHUTDOWN_SIMULATED:
        logger.warning('shutting down system NOW')
        subprocess.run(
            'sudo sh -c "echo heartbeat > /sys/class/leds/led1/trigger ; systemctl poweroff &"',
            shell=True
        )

    else:
        logger.info('system shutdown dry-run')


class SystemShutdownHandler(tornado.web.RequestHandler):
    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        if os.getuid() in (0, 1000) and data.get('confirmation_code') == config.SHUTDOWN_CONFIRMATION_CODE:
            # give an audible feedback by turning the fan on
            cooling_controller: CoolingController = self.application.settings['cooling_controller']
            cooling_controller.set_mode(CoolingMode.ON)

            self.set_status(200)
            logger.info("shutting down system in %s seconds...", config.SHUTDOWN_DELAY)

            tornado.ioloop.IOLoop.current().call_later(config.SHUTDOWN_DELAY, _shutdown_system)

        else:
            self.set_status(403)


class SystemTemperatureHandler(tornado.web.RequestHandler):
    def get(self):
        cooling = self.application.settings['cooling_controller']
        self.write(json.dumps({
            "temp": cooling.get_core_temperature()
        }))
        self.set_header("Cache-control", "no-store")


class ConfigurationHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(json.dumps({
            "detection_threshold": config.SYMBOLS_MATCH_THRESHOLD,
            "min_contour_width": config.MIN_CONTOUR_WIDTH,
            "min_contour_height": config.MIN_CONTOUR_HEIGHT,
        }))


class SettingsHandler(tornado.web.RequestHandler):
    def get(self):
        user_settings: UserSettings = self.application.settings['user_settings']
        self.write(json.dumps(user_settings.as_dict()))

    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        logger.info("save cooling settings: %s", data)
        user_settings: UserSettings = self.application.settings['user_settings']
        cooling_mode = data["cooling_mode"]
        user_settings.update(
            cooling_mode=cooling_mode,
            cooling_auto_on=data["cooling_auto_on"],
            cooling_auto_off=data["cooling_auto_off"],
        )
        user_settings.save()

        cooling_controller = self.application.settings['cooling_controller']
        cooling_controller.set_mode(cooling_mode)

        self.set_status(201)


class CameraSettingsHandler(tornado.web.RequestHandler):
    def get(self):
        camera = CameraManager.get_camera()
        self.write(json.dumps({
            'expo_digital_gain': frac_to_float(camera.digital_gain),
            'expo_analog_gain': frac_to_float(camera.analog_gain),
            'awb_red_gain': frac_to_float(camera.awb_gains[0]),
            'awb_blue_gain': frac_to_float(camera.awb_gains[1]),
        }))


class CameraCalibrationHandler(tornado.web.RequestHandler):
    def post(self):
        camera = CameraManager.recreate_camera()
        self.write(json.dumps({
            'expo_digital_gain': frac_to_float(camera.digital_gain),
            'expo_analog_gain': frac_to_float(camera.analog_gain),
            'awb_red_gain': frac_to_float(camera.awb_gains[0]),
            'awb_blue_gain': frac_to_float(camera.awb_gains[1]),
        }))


class AboutHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(
            "about.html",
            current_page="about",
            **app_context
        )


class OpenCVHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(
            "cv.html",
            current_page="cv",
            **app_context
        )


class OpenCVImgHandler(PipelineMixin, tornado.web.RequestHandler):
    def get(self):
        if self._pipeline.state == State.UNDEFINED:
            self.set_status(421)
            return

        _, encoded_img = cv2.imencode('.jpg', self._pipeline.image_display)
        self.write(encoded_img.tostring())
        self.set_header("Content-type", "image/jpg")
        self.set_header("Cache-control", "no-store")


class CVWebSocket(ImageCaptureMixin, PipelineMixin, tornado.websocket.WebSocketHandler):
    def open(self):
        logger.info("CVWebSocket opened")
        self._pipeline.reset()

    def on_message(self, message):
        domain, command = message.split(':')
        if domain.lower() == 'cv':
            self.process_cv_command(command.lower())
        else:
            self.write_message("ERR:invalid command (%s)" % command)

    def on_close(self):
        logger.info("CVWebSocket closed")

    def process_cv_command(self, command):
        cmde_is_valid = True

        if command == 'capture':
            image_path = self.acquire_image()
            logger.info(f"initializing pipeline with image {image_path}, step mode=True")
            self._pipeline.initialize(image_path, display_steps=True)

        elif command == 'step':
            self._pipeline.step()

            if self._pipeline.state == State.COMPLETE:
                self._pipeline.log_program()

        elif command == 'reset':
            self._pipeline.reset()

        else:
            cmde_is_valid = False

        if cmde_is_valid:
            if self._pipeline.state != State.ERROR:
                self.write_message('OK:%s' % self._pipeline.state)
            else:
                self.write_message('ERR:%s' % self._pipeline.error)
        else:
            self.write_message("ERR:invalid CV command (%s)" % command)


class BotEventsWebSocket(BotMixin, tornado.websocket.WebSocketHandler):
    def open(self):
        logger.info("BotEventsWebSocket opened")
        self.ws_opened(self)

    def on_close(self):
        logger.info("BotEventsWebSocket closed")
        self.ws_closed()


def make_app():
    pipeline = CVPipeline(
        match_threshold=config.SYMBOLS_MATCH_THRESHOLD,
        logger=get_logger('pipeline'),
    )

    user_settings = UserSettings.get()

    cooling_controller = CoolingController(user_settings.cooling, get_logger('cooling'))

    bot = Bot(port=config.BOT_SERIAL_PORT, baudrate=config.BOT_SERIAL_BAUD)

    log_settings("CVPipeline settings", pipeline.get_settings())

    return tornado.web.Application([
        (r"/", HomeHandler),
        (r"/about", AboutHandler),

        (r"/prog$", ProgrammingHandler),
        (r"/prog/analyze", ProgramAnalyzeHandler, {'pipeline': pipeline}),
        (r"/prog/img", ProgramBoardImageHandler, {'pipeline': pipeline}),
        (r"/prog/execute", BotMoveHandler, {'bot': bot}),
        (r"/prog/stop", BotStopHandler, {'bot': bot}),

        (r"/bot", BotControlHandler, {'bot': bot}),
        (r"/bot/move", BotMoveHandler, {'bot': bot}),
        (r"/bot/stop", BotStopHandler, {'bot': bot}),
        (r"/bot/state", BotGetStateHandler, {'bot': bot}),
        (r"/bot/sensors", BotGetSensorsHandler, {'bot': bot}),
        (r"/bot/obst", BotGetObstacleHandler, {'bot': bot}),

        (r"/cv$", OpenCVHandler),
        (r"/cv/img", OpenCVImgHandler, {'pipeline': pipeline}),
        (r"/cv/refs/(.*)", tornado.web.StaticFileHandler, {'path': os.path.join(__here__, 'data/refs/')}),

        (r"/sys$", SystemCommandsHandler),
        (r"/sys/shutdown$", SystemShutdownHandler),
        (r"/sys/temp", SystemTemperatureHandler),
        (r"/sys/config", ConfigurationHandler),
        (r"/sys/settings", SettingsHandler),
        (r"/sys/camera", CameraSettingsHandler),
        (r"/sys/calibrate", CameraCalibrationHandler),

        (r"/ws/cv", CVWebSocket, {'pipeline': pipeline}),
        (r"/ws/evt", BotEventsWebSocket, {'bot': bot}),
    ],
        static_path=os.path.join(pkg_dir, 'static'),
        template_path=os.path.join(pkg_dir, 'templates'),
        debug=config.DEBUG,
        bot=bot,
        user_settings=user_settings,
        cooling_controller=cooling_controller
    )


HSEP = '-' * 80

http_client = tornado.httpclient.AsyncHTTPClient()


def runserver(port=80):
    app_logger.setLevel(logging.DEBUG if config.DEBUG else logging.INFO)

    logger.info(HSEP)
    logger.info('Starting server...')
    logger.info(HSEP)

    log_settings("Configuration", config.env.dump())

    logger.info('OpenCV version     : %s', cv2.__version__)
    logger.info('OpenCV-python path : %s', Path(cv2.__file__))
    app = make_app()

    webapp_settings = {k: v for k, v in app.settings.items() if k != 'bot'}
    webapp_settings['port'] = port
    log_settings("Web app settings", webapp_settings)

    log_settings("User settings", app.settings["user_settings"].as_dict())

    try:
        app.listen(port)
    except PermissionError as e:
        logger.error('cannot bind to port %d (maybe already in use)', port)
        logger.error('cause: %s', e)
        return

    io_loop = tornado.ioloop.IOLoop.current()
    bot = app.settings['bot']
    try:
        bot.ready(io_loop)

        logger.info(HSEP)
        logger.info("Server started and listening on port %d...", port)
        logger.info(HSEP)

        io_loop.start()

    except KeyboardInterrupt:
        print()
        logger.info("Server stopped by external signal.")

    finally:
        bot.shutdown()

        logger.info(HSEP)
        logger.info("Server terminated.")
        logger.info(HSEP)
