from dataclasses import dataclass
from enum import Enum
import subprocess
from pathlib import Path
from typing import Union

from tornado.ioloop import PeriodicCallback

from . import config


class CoolingMode(Enum):
    OFF = 'off'
    ON = 'on'
    AUTO = 'auto'


@dataclass()
class Thresholds:
    on: int
    off: int


@dataclass()
class CoolingSettings:
    mode: CoolingMode
    auto_mode_thresholds: Thresholds = Thresholds(on=50, off=35)


USB_ID = "1-1"
USB_STATUS_PATH = "/sys/bus/usb/drivers/usb/%s/power/runtime_status"
BIND_PATH = "/sys/bus/usb/drivers/usb/bind"
UNBIND_PATH = "/sys/bus/usb/drivers/usb/unbind"


class CoolingController:
    _current_mode: CoolingMode

    def __init__(self, settings: CoolingSettings, logger, usb_id: str = USB_ID):
        self._usb_id = usb_id
        self._monitor: Union[PeriodicCallback, None] = None
        self._thresholds = settings.auto_mode_thresholds
        self._thermal_zone = config.CPU_THERMAL_ZONE
        self._sentinel_path = Path(USB_STATUS_PATH % self._usb_id)
        self._logger = logger

        self.set_mode(settings.mode)

    def is_on(self) -> bool:
        return self._sentinel_path.exists()

    def turn_on(self):
        if not self.is_on():
            self._logger.info("turning fan on")
            subprocess.run(
                f"""sudo sh -c 'echo "{self._usb_id}" > {BIND_PATH}'""",
                shell=True
            )

    def turn_off(self):
        if self.is_on():
            self._logger.info("turning fan off")
            subprocess.run(
                f"""sudo sh -c 'echo "{self._usb_id}" > {UNBIND_PATH}'""",
                shell=True
            )

    def set_mode(self, mode: Union[str, CoolingMode]):
        if isinstance(mode, str):
            mode = CoolingMode(mode.lower())

        if mode == CoolingMode.ON:
            self.uninstall_monitor()
            self.turn_on()
        elif mode == CoolingMode.OFF:
            self.uninstall_monitor()
            self.turn_off()
        elif mode == CoolingMode.AUTO:
            self.install_monitor()
        else:
            raise ValueError()

        self._current_mode = mode
        self._logger.info("mode set %s", self._current_mode)

    def set_thresholds(self, thresholds: Thresholds):
        self._thresholds = thresholds

    def get_core_temperature(self) -> float:
        dev_path = Path(f"/sys/class/thermal/thermal_zone{self._thermal_zone}/temp")
        return round(int(dev_path.read_text().strip()) / 1000, 1)


    def install_monitor(self):
        if not self._monitor:
            self._monitor = PeriodicCallback(self._control_cooling, 10_000)
            self._monitor.start()

    def uninstall_monitor(self):
        if self._monitor:
            self._monitor.stop()
            self._monitor = None

    def _control_cooling(self):
        core_temp = self.get_core_temperature()
        self._logger.debug(
            "checking core temp (%.1f°C) with thresholds (%.1f°C - %.1f°C)",
            core_temp, self._thresholds.off, self._thresholds.on
        )
        if core_temp > self._thresholds.on:
            self.turn_on()
        elif core_temp < self._thresholds.off:
            self.turn_off()
