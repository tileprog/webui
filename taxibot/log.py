import logging

logging.basicConfig(
    format="%(asctime)s [%(levelname).1s] <%(name)s> %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO
)

app_logger = logging.getLogger()


def get_logger(name: str):
    return app_logger.getChild(name)


def log_settings(title: str, data: dict):
    app_logger.info("%s :", title)
    for k, v in sorted(data.items()):
        app_logger.info("| - %-40s : %s", k, v)
    app_logger.info('+---')
